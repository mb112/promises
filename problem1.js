const fs = require('fs')
const path = require("path");

async function getToDoList(url) {
    const toDo = await fetch(url).then(res => res.json())
    return toDo
}

module.exports = getToDoList

const resultsToDump = {}

const c = getToDoList('https://jsonplaceholder.typicode.com/todos').then((res) => {
    return res.reduce((acc, current) => {
        if (acc[current.userId]) {
            if (current.completed === true) {
                acc[current.userId].unshift(current)
                return acc
            } else {
                acc[current.userId].push(current)
                return acc
            }
        } else {
            acc[current.userId] = [current]
            return acc
        }
    }, {})
}).then(result => resultsToDump['C']=result)


const d = getToDoList('https://jsonplaceholder.typicode.com/todos').then(res => {
    return res.reduce((acc, current) => {
        if (current.completed === true) {
            acc.completed.push(current)
            return acc
        } else {
            acc.noncompleted.push(current)
            return acc
        }
    }, {completed: [], noncompleted: []})
}).then(result => resultsToDump['D']=result)

async function dumpResults(resultFile){
    await c
    await d
    let result = JSON.stringify(resultsToDump)
    fs.appendFile(path.join(__dirname,resultFile),result,(err,_)=> {
        if (err) {
            console.log(err)
        }
    })
}


dumpResults('results_problem1.json')




