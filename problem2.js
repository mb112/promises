const fs = require('fs')
const path = require("path");


function readFile(filename,method) {
    return new Promise((resolve,reject)=>{
        fs.readFile(path.join(__dirname,filename),"utf-8",(err,data)=>{
            if(err){
                reject(err)
            }else {
                let results = JSON.parse(data)
                resolve(method(results))
            }
        })
    })
}

function transformData(data){
    return Object.values(data)

}


readFile('results_problem1.json',transformData).then(res => console.log(res)).catch(err =>{
    console.log('Missing File')
})