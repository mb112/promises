function getIdList(...userID) {
    let userList = userID.reduce((acc, curr) => {
        acc = acc +'id=' +curr + '&'
        return acc
    }, '')
    return userList
}

let list = getIdList(4, 6, 2)
let details = fetch('https://jsonplaceholder.typicode.com/users?' + list).then(data => data.json())

Promise.all([details]).then(result => console.log(result))
