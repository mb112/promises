const fs = require('fs')
const getToDoList = require('./problem1')

async function getUserList(url) {
    let users = await fetch(url).then(res => res.json())
    return users
}

const Nicholas = getUserList('https://jsonplaceholder.typicode.com/users').then(res => {
    return res.reduce((acc, current) => {
        if (current.name.includes('Nicholas')) {
            acc[current.id]= 'Nicholas'
            return acc
        }
        return acc
    }, {})
}).then(result => {
   return  getToDoList('https://jsonplaceholder.typicode.com/todos').then(todoList => {
       let list = todoList.reduce((acc, curr) => {
            if (result[curr.userId]) {
                acc.push(curr)
                return acc
            }
            return acc
        }, [])
        return list
    })
}).then (finalresult => console.log(finalresult))





